package com.company.ToBeTested;

public class EvenChecker {

    public boolean isEven(int num){
        return num % 2 == 0;
    }
    public boolean isEven(String num){
        try{
            return isEven(Integer.parseInt(num));
        }
        catch (NumberFormatException e) {
            throw new NumberFormatException("Please enter a valid number");
        }
    }
}
