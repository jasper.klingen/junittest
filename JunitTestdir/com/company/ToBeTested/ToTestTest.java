package com.company.ToBeTested;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToTestTest {
    @Test
    void toTest_oneAndOne_isTwo(){
        var toTest = new ToTest();
        assertEquals(2, toTest.Calc(1,1));
    }
}