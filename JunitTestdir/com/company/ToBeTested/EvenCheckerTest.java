package com.company.ToBeTested;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EvenCheckerTest {

    @Test
    void isNumberEven_evenNumber_true(){
        var evenChecker = new EvenChecker();
        int num = 2;
        assertTrue(evenChecker.isEven(num));
    }

    @Test
    void isNumberEven_oddNumber_false(){
        var evenChecker = new EvenChecker();
        int num = 1;
        assertFalse(evenChecker.isEven(num));
    }

    @Test
    void isStringParseEven_stringContainsEvenNumber_true(){
        var evenChecker = new EvenChecker();
        assertTrue(evenChecker.isEven("2"));
    }

    @Test
    void isStringParseEven_invalidString_throwsNumberFormatException(){
        var evenChecker = new EvenChecker();
        assertThrows(NumberFormatException.class, () -> evenChecker.isEven("Two"));
    }
}